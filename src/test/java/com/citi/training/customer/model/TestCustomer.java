package com.citi.training.customer.model;

import org.junit.Test;

public class TestCustomer {

	@Test
	public void testGetterSetter() {
		Customer c = new Customer(1, "Eamonn", "64 Zoo Lane");
		assert(c.getId() == 1);
		assert(c.getName() == "Eamonn");
		assert(c.getAddress() == "64 Zoo Lane");
	}

}
