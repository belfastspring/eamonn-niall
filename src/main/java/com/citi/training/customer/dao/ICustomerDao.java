package com.citi.training.customer.dao;

import java.util.List;

import com.citi.training.customer.model.Customer;

public interface ICustomerDao {

	void saveCustomer(Customer customer);
	Customer getCustomer(int id);
	List<Customer> getAllCustomers();
	void removeCustomer(int id);
}
